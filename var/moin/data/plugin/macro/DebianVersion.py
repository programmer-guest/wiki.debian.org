from MoinMoin import wikiutil

suites = set(['oldstable', 'stable', 'testing', 'unstable', 'experimental'])

def execute(macro, suite):
	if suite in suites:
		with open('/srv/wiki.debian.org/var/apt/%s.version' % suite, 'r') as version_file:
			return macro.formatter.rawHTML(wikiutil.escape(version_file.read().strip()))
	return ''
