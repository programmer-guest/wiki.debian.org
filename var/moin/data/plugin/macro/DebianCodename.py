from MoinMoin import wikiutil

suites = set(['oldstable', 'stable', 'testing', 'unstable', 'experimental'])

def execute(macro, suite):
	if suite in suites:
		with open('/srv/wiki.debian.org/var/apt/%s.codename' % suite, 'r') as codename_file:
			return macro.formatter.rawHTML(wikiutil.escape(codename_file.read().strip()))
	return ''
