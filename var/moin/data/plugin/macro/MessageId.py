from MoinMoin import wikiutil

def execute(macro, id):
	id = wikiutil.escape(id)
	html = u'&lt;<a href="https://lists.debian.org/%(id)s">%(id)s</a>&gt;' % {'id': id}
	return macro.formatter.rawHTML(html)
